package cz.koto.misak.kotipoint.android.mobile;


public class KoTiPointConfig
{

	public static final String SSL_CERTIFICATE_SHA1 = "myhash="; // encoded representation of the trusted certificate
	public static final String SSL_KEYSTORE_PASSWORD = "mypassword"; // password for BKS keystore, generated via script: https://github.com/petrnohejl/Android-Scripts/blob/master/certkey.bat
}
