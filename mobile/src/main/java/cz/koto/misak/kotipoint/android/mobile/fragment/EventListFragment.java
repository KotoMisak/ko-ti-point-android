package cz.koto.misak.kotipoint.android.mobile.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import cz.koto.misak.kotipoint.android.base.client.rest.KoTiNodeClient;
import cz.koto.misak.kotipoint.android.base.entity.KoTiEvent;
import cz.koto.misak.kotipoint.android.base.utility.Logcat;
import cz.koto.misak.kotipoint.android.mobile.R;
import cz.koto.misak.kotipoint.android.mobile.adapter.EventListRecyclerAdapter;
import cz.koto.misak.kotipoint.android.mobile.entity.AppPermissionEnum;
import cz.koto.misak.kotipoint.android.mobile.utility.NetworkUtility;
import cz.koto.misak.kotipoint.android.mobile.view.LinearDividerItemDecoration;
import cz.koto.misak.kotipoint.android.mobile.view.StatefulLayout;
import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;


public class EventListFragment extends TaskFragment implements EventListRecyclerAdapter.EventViewHolder.OnItemClickListener {

    private View mRootView;
    private StatefulLayout mStatefulLayout;
    private EventListRecyclerAdapter mAdapter;
    private List<KoTiEvent> mEventList = new ArrayList<>();

    private CompositeSubscription mSuscription = new CompositeSubscription();

    public static EventListFragment newInstance() {
        EventListFragment fragment = new EventListFragment();
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        setRetainInstance(true);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_event_list_recycler, container, false);
        setupRecyclerView();
        return mRootView;
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // setup stateful layout
        setupStatefulLayout(savedInstanceState);

        /*
         * Request all permissions defined in getPermissionList and
         * call doWithPermissions() after all of them are granted.
         */
        requestPermissions();

    }

    private void doLoading() {
        // load data
        if (mStatefulLayout.getState() == null) loadData();
    }


    @Override
    public void onStart() {
        super.onStart();
    }


    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public void onPause() {
        super.onPause();

        // stop adapter
        if (mAdapter != null) mAdapter.stop();
    }


    @Override
    public void onStop() {
        super.onStop();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mRootView = null;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();

        // cancel async tasks
        //if (mLoadDataTask != null) mLoadDataTask.cancel(true);
    }


    @Override
    public void onDetach() {
        super.onDetach();
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        // save current instance state
        super.onSaveInstanceState(outState);
        setUserVisibleHint(true);

        // stateful layout state
        if (mStatefulLayout != null) mStatefulLayout.saveInstanceState(outState);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // action bar menu
        super.onCreateOptionsMenu(menu, inflater);

        // TODO
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // action bar menu behavior
        return super.onOptionsItemSelected(item);

        // TODO
    }


    @Override
    public void onItemClick(View view, int position, long id, int viewType) {
        // position

        // TODO
    }


    @Override
    public void onItemLongClick(View view, int position, long id, int viewType) {
        // position

        // TODO
    }


    private void loadData() {
        if (NetworkUtility.isOnline(getActivity())) {
            // show progress
            mStatefulLayout.showProgress();

            Observable<List<KoTiEvent>> events = KoTiNodeClient.getKoTiNodeClient().eventList();
            mSuscription.add(
                    events
                            .subscribeOn(Schedulers.io())//thread where your operators will run
                            .observeOn(AndroidSchedulers.mainThread())//what thread your observer will run on
                            .subscribe(new Observer<List<KoTiEvent>>() {
                                @Override
                                public void onCompleted() {

                                    Log.d("-", "Retrofit call successful");
                                    mAdapter.notifyItemInserted(mAdapter.getItemCount());
                                    mStatefulLayout.showContent();
                                }

                                @Override
                                public void onError(Throwable e) {
                                    Log.i("Api call failed", null, e);
                                    Snackbar.make(mRootView, getContext().getResources().getText(R.string.server_connection_error), Snackbar.LENGTH_LONG).show();
                                    mStatefulLayout.showEmpty();
                                }

                                @Override
                                public void onNext(List<KoTiEvent>
                                                           eventList) {
                                    for (KoTiEvent event : eventList) {
                                        mAdapter.addKoTiEvent(event);
                                    }
                                }
                            }));
        } else {
            mStatefulLayout.showOffline();
        }
    }

    private void setupStatefulLayout(Bundle savedInstanceState) {
        // reference
        mStatefulLayout = (StatefulLayout) mRootView;

        // state change listener
        mStatefulLayout.setOnStateChangeListener(new StatefulLayout.OnStateChangeListener() {
            @Override
            public void onStateChange(View v, StatefulLayout.State state) {
                Logcat.d("" + (state == null ? "null" : state.toString()));

                if (state == StatefulLayout.State.CONTENT) {


                }
            }
        });

        // restore state
        mStatefulLayout.restoreInstanceState(savedInstanceState);
    }


    private RecyclerView getRecyclerView() {
        return mRootView != null ? (RecyclerView) mRootView.findViewById(R.id.fragment_event_list_recycler) : null;
    }


    private void setupRecyclerView() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        RecyclerView recyclerView = getRecyclerView();
        recyclerView.setLayoutManager(linearLayoutManager); // TODO: use LinearLayoutManager, GridLayoutManager or StaggeredGridLayoutManager

        // content
        if (recyclerView.getAdapter() == null) {
            // create adapter
            mAdapter = new EventListRecyclerAdapter(mEventList, this);
        } else {
            // refill adapter
            mAdapter.refill(mEventList, this);
        }

        // set fixed size
        recyclerView.setHasFixedSize(true);

        // add decoration
        RecyclerView.ItemDecoration itemDecoration = new LinearDividerItemDecoration(getActivity(), null);
        recyclerView.addItemDecoration(itemDecoration);

        // set animator
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        // set adapter
        recyclerView.setAdapter(mAdapter);

        // lazy loading
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }


            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                int firstVisibleItem = layoutManager.findFirstVisibleItemPosition();
                int visibleItemCount = layoutManager.getChildCount();
                int totalItemCount = layoutManager.getItemCount();
                int lastVisibleItem = firstVisibleItem + visibleItemCount;

//                if (totalItemCount - lastVisibleItem <= LAZY_LOADING_OFFSET && mEventList.size() % LAZY_LOADING_TAKE == 0 && !mEventList.isEmpty()) {
//                    if (!mLazyLoading) lazyLoadData();
//                }
            }
        });
    }

    @Override
    public void doWithPermissions() {
        doLoading();
    }

    @Override
    public void permissionNotGranted() {
        mStatefulLayout.setState(StatefulLayout.State.EMPTY);
    }

    @Override
    public List<AppPermissionEnum> getPermissionList() {
        return null;//Arrays.asList(AppPermissionEnum.LOCATION);
    }


}
