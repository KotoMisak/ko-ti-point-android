package cz.koto.misak.kotipoint.android.mobile.activity;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import cz.koto.misak.kotipoint.android.mobile.R;
import cz.koto.misak.kotipoint.android.mobile.fragment.EventDetailFragment;
import cz.koto.misak.kotipoint.android.mobile.fragment.EventListFragment;


public class MainActivity  extends AppCompatActivity
{
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private ListView mDrawerListView;
    private CharSequence mTitle;
    private CharSequence mDrawerTitle;
    private String[] mTitles;


    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupActionBar();
        setupDrawer(savedInstanceState);
    }


    @Override
    public void onStart()
    {
        super.onStart();
    }


    @Override
    public void onResume()
    {
        super.onResume();
    }


    @Override
    public void onPause()
    {
        super.onPause();
    }


    @Override
    public void onStop()
    {
        super.onStop();
    }


    @Override
    public void onDestroy()
    {
        super.onDestroy();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // action bar menu
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.activity_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu)
    {
        // action bar menu visibility
        if(menu!=null)
        {
            boolean drawerOpened = mDrawerLayout.isDrawerOpen(mDrawerListView);
            MenuItem refresh = menu.findItem(R.id.menu_activity_main_refresh);
            if(refresh!=null) refresh.setVisible(!drawerOpened);
        }
        return super.onPrepareOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // open or close the drawer if home button is pressed
        if(mDrawerToggle.onOptionsItemSelected(item))
        {
            return true;
        }

        // action bar menu behavior
        switch(item.getItemId())
        {
            case android.R.id.home:
                // TODO
                return true;

            case R.id.menu_activity_main_refresh:
                // TODO
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState)
    {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }


    @Override
    public void onConfigurationChanged(Configuration newConfiguration)
    {
        super.onConfigurationChanged(newConfiguration);
        mDrawerToggle.onConfigurationChanged(newConfiguration);
    }


    @Override
    public void onBackPressed()
    {
        if(mDrawerLayout.isDrawerOpen(Gravity.LEFT))
        {
            mDrawerLayout.closeDrawer(Gravity.LEFT);
        }
        else
        {
            super.onBackPressed();
        }
    }


    @Override
    public void setTitle(CharSequence title)
    {
        mTitle = title;
        getSupportActionBar().setTitle(mTitle);
    }


    private void setupActionBar()
    {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar bar = getSupportActionBar();
        bar.setDisplayUseLogoEnabled(false);
        bar.setDisplayShowTitleEnabled(true);
        bar.setDisplayShowHomeEnabled(true);
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setHomeButtonEnabled(true);
    }


    private void setupDrawer(Bundle savedInstanceState)
    {
        mTitle = getTitle();
        mDrawerTitle = getTitle();

        mTitles = new String[2];
        mTitles[0] = getString(R.string.title_news);
        mTitles[1] = getString(R.string.title_forum);


        Integer[] icons = new Integer[4];
        icons[0] = R.drawable.ic_menu_news;
        icons[1] = R.drawable.ic_menu_forum;


        // reference
        mDrawerLayout = (DrawerLayout) findViewById(R.id.activity_example_drawer_layout);
        mDrawerListView = (ListView) findViewById(R.id.activity_example_drawer_list);

        // set drawer
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        mDrawerListView.setAdapter(new cz.koto.misak.kotipoint.android.mobile.adapter.DrawerAdapter(this, mTitles, icons));
        mDrawerListView.setOnItemClickListener(new OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View clickedView, int position, long id)
            {
                selectDrawerItem(position);
            }
        });
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.drawer_open, R.string.drawer_close)
        {
            @Override
            public void onDrawerClosed(View view)
            {
                getSupportActionBar().setTitle(mTitle);
                supportInvalidateOptionsMenu();
            }

            @Override
            public void onDrawerOpened(View drawerView)
            {
                getSupportActionBar().setTitle(mDrawerTitle);
                supportInvalidateOptionsMenu();
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        // show initial fragment
        if(savedInstanceState == null)
        {
            selectDrawerItem(0);
        }
    }


    private void selectDrawerItem(int position)
    {
        Fragment fragment;
        switch (position){
            default:
            case 0: fragment = EventListFragment.newInstance(); break;
            case 1: fragment = EventDetailFragment.newInstance(); break;
        }

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.container_drawer_content, fragment).commitAllowingStateLoss();

        mDrawerListView.setItemChecked(position, true);
        setTitle(mTitles[position]);
        mDrawerLayout.closeDrawer(mDrawerListView);
    }
}
