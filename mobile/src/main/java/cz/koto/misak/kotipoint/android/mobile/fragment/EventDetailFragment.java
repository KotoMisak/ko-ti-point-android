package cz.koto.misak.kotipoint.android.mobile.fragment;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import cz.koto.misak.kotipoint.android.mobile.R;
import cz.koto.misak.kotipoint.android.mobile.dialog.ProgressDialogFragment;


public class EventDetailFragment extends Fragment {

	private static final String DIALOG_PROGRESS = "progress";
	
	private View mRootView;

    public static EventDetailFragment newInstance()
    {
        EventDetailFragment fragment = new EventDetailFragment();
        return fragment;
    }
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		mRootView = inflater.inflate(R.layout.fragment_event_detail, container, false);
		return mRootView;
	}
	
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{
		super.onActivityCreated(savedInstanceState);
		bindData();
	}



	
	private void bindData()
	{
		// reference

		// content

	}
	

	
//	private void showProgressDialog()
//	{
//		// create dialog
//		DialogFragment newFragment = ProgressDialogFragment.newInstance();
//		newFragment.setTargetFragment(this, 0);
//		newFragment.show(getFragmentManager(), DIALOG_PROGRESS);
//	}
//
//
//	private void hideProgressDialog()
//	{
//		getFragmentManager().executePendingTransactions();
//		DialogFragment dialogFragment = (DialogFragment) getFragmentManager().findFragmentByTag(DIALOG_PROGRESS);
//		if(dialogFragment != null) dialogFragment.dismiss();
//	}
}
