package cz.koto.misak.kotipoint.android.mobile.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import cz.koto.misak.kotipoint.android.base.entity.KoTiEvent;
import cz.koto.misak.kotipoint.android.mobile.R;


public class EventListRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int VIEW_TYPE_EVENT = 1;

    private List<KoTiEvent> mEventList = new ArrayList<>();
    private EventViewHolder.OnItemClickListener mListener;


    public EventListRecyclerAdapter(List<KoTiEvent> eventList, EventViewHolder.OnItemClickListener listener) {
        mEventList = eventList == null ? new ArrayList<KoTiEvent>() : eventList;
        mListener = listener;
    }

    public void addKoTiEvent(KoTiEvent event) {
        mEventList.add(event);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        if (viewType == VIEW_TYPE_EVENT) {
            View view = inflater.inflate(R.layout.fragment_event_list_recycler_item, parent, false);
            return new EventViewHolder(view, mListener);
        } else {
            throw new RuntimeException("There is no view type that matches the type " + viewType);
        }
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        if (viewHolder instanceof EventViewHolder) {
            // entity
            KoTiEvent koTiEvent = mEventList.get(position);

            // bind data
            if (koTiEvent != null) {
                ((EventViewHolder) viewHolder).bindData(koTiEvent);
            }
        }
    }


    @Override
    public int getItemCount() {
        int size = 0;
        if (mEventList != null) size += mEventList.size();
        return size;
    }


    @Override
    public int getItemViewType(int position) {
//        int products = mEventList.size();
//        if (position < headers) return VIEW_TYPE_HEADER;
//        else if (position < headers + products) return VIEW_TYPE_EVENT;
//        else if (position < headers + products + footers) return VIEW_TYPE_FOOTER;
//        else return -1;
        return VIEW_TYPE_EVENT;
    }



    public void refill(List<KoTiEvent> eventList, EventViewHolder.OnItemClickListener listener) {
        mEventList = eventList == null ? new ArrayList<KoTiEvent>() : eventList;
        mListener = listener;
        notifyDataSetChanged();
    }


    public void stop() {
        // TODO: stop image loader
    }


    public static final class EventViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        private TextView mNameTextView;
        private OnItemClickListener mListener;


        public interface OnItemClickListener {
            void onItemClick(View view, int position, long id, int viewType);

            void onItemLongClick(View view, int position, long id, int viewType);
        }


        public EventViewHolder(View itemView, OnItemClickListener listener) {
            super(itemView);
            mListener = listener;

            // set listener
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);

            // find views
            mNameTextView = (TextView) itemView.findViewById(R.id.fragment_event_list_recycler_item_name);
        }


        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {
                mListener.onItemClick(view, position, getItemId(), getItemViewType());
            }
        }


        @Override
        public boolean onLongClick(View view) {
            int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {
                mListener.onItemLongClick(view, position, getItemId(), getItemViewType());
            }
            return true;
        }


        public void bindData(KoTiEvent koTiEvent) {
            mNameTextView.setText(koTiEvent.getmHeadline());
        }
    }

}
