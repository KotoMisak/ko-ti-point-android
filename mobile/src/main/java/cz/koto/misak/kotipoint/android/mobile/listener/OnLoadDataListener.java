package cz.koto.misak.kotipoint.android.mobile.listener;


public interface OnLoadDataListener
{
	void onLoadData();
}
