# ko-ti-point-android

![KoTiLogo](./mobile/src/main/res/mipmap-hdpi/ic_launcher.png "KoTiLogo")

[KoTiProject overview](http://kotopeky.cz/project)

KoTiPoint application is android based communication channel for those, they want to stay connected with Kotopeky and Tihava villages.


Features
============

Release Notes
============
0.1.x.x - Introduce empty project
0.2.x.x - Event list feature


Release Plan
============
This KoTiPoint app is new version of original [KoTiPoint](https://github.com/kotomisak/koti-point-android) idea, 
but with different way of thinking about code architecture.

                                                                                               
0.b.x.x - Media feature

0.c.x.x - Forum feature                                                                                               
                                                                                               
Documentation
=============


Dependencies
============

Testing
=======

Developed by
============

[Michal Jeníček](https://www.linkedin.com/in/jenicekmichal)


License
=======

        Copyleft ©2015 Michal Jenicek

Screens
============


