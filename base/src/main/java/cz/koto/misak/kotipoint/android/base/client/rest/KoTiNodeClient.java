package cz.koto.misak.kotipoint.android.base.client.rest;

import android.content.Context;
import android.content.res.Resources;

import com.google.gson.GsonBuilder;

import cz.koto.misak.kotipoint.android.base.KoTiPointBaseConfig;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;
import retrofit.RxJavaCallAdapterFactory;

public class KoTiNodeClient {

    private static KoTiNodeApi restInterface;

    public static KoTiNodeApi getKoTiNodeClient() {

        if (restInterface == null) {
            GsonBuilder gsonBuilder = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.sss'Z'");//ISO-8601
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(KoTiPointBaseConfig.API_ENDPOINT_PRODUCTION)
                    .addConverterFactory(GsonConverterFactory.create(gsonBuilder.create()))
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())//important for RX!!!
                    .build();

            restInterface =  retrofit.create(KoTiNodeApi.class);
        }
        return restInterface;
    }
}