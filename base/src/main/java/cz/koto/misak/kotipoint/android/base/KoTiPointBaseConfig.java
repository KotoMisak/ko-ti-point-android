package cz.koto.misak.kotipoint.android.base;


public class KoTiPointBaseConfig
{

	public static final boolean LOGS = BuildConfig.LOGS;
	public static final boolean DEV_API = BuildConfig.DEV_API;

	public static final String API_ENDPOINT_PRODUCTION = "http://kotopeky.cz/api/kotinode";
	public static final String API_ENDPOINT_DEV_EMULATOR = "http://10.0.2.2:8080";
	public static final String API_ENDPOINT_DEV_VBOXNET = "http://10.0.3.2:8080";
}
