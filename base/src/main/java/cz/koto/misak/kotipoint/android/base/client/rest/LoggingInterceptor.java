package cz.koto.misak.kotipoint.android.base.client.rest;

import android.util.Log;

import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;

import cz.koto.misak.kotipoint.android.base.utility.Logcat;


class LoggingInterceptor implements Interceptor {

    public LoggingInterceptor(){
        Logcat.i("Communication interceptor instantiated!");
    }

    @Override
    public Response intercept(Chain chain){
        try {
            Request request = chain.request();

            long t1 = System.nanoTime();
            Logcat.i(String.format("Sending request %s on %s%n%s",
                    request.url(), chain.connection(), request.headers()));

            Response response = chain.proceed(request);

            long t2 = System.nanoTime();
            Logcat.i(String.format("Received response for %s in %.1fms%n%s",
                    response.request().url(), (t2 - t1) / 1e6d, response.headers()));

            return response;

        }catch (Throwable th){
            Logcat.e(th,"Unable to intercept internet communication.");
            return null;
        }
    }
}