package cz.koto.misak.kotipoint.android.base.entity;



import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 "headline": "Bazárek v Praskolesích",
 "label":null,
 "eventDate":"2015-10-31T00:00:00.000Z",
 "eventLocation":"Praskolesy",
 "textCapital":"Chcete nabídnout nepotřebné věci?",
 "text": "Kulturní dům Jitřenka 8:00, prodejní poplatek za stůl je 20,-",
 "imageResource":""
 */
public class KoTiEvent
{

    @SerializedName("headline")
    String mHeadline;

    @SerializedName("label")
    List<String> mLabel;

    @SerializedName("eventDate")
    Date mEventDate;

    @SerializedName("eventLocation")
    List<String> mEventLocation;

    @SerializedName("textCapital")
    String mTextCapital;

    @SerializedName("text")
    String mText;

    @SerializedName("imageResource")
    String mImageResource;


    public KoTiEvent() {
    }

    public String getmHeadline() {
        return mHeadline;
    }

    public void setmHeadline(String mHeadline) {
        this.mHeadline = mHeadline;
    }

    public List<String> getmLabel() {
        return mLabel;
    }

    public void setmLabel(List<String> mLabel) {
        this.mLabel = mLabel;
    }

    public Date getmEventDate() {
        return mEventDate;
    }

    public void setmEventDate(Date mEventDate) {
        this.mEventDate = mEventDate;
    }

    public List<String> getmEventLocation() {
        return mEventLocation;
    }

    public void setmEventLocation(List<String> mEventLocation) {
        this.mEventLocation = mEventLocation;
    }

    public String getmTextCapital() {
        return mTextCapital;
    }

    public void setmTextCapital(String mTextCapital) {
        this.mTextCapital = mTextCapital;
    }

    public String getmText() {
        return mText;
    }

    public void setmText(String mText) {
        this.mText = mText;
    }

    public String getmImageResource() {
        return mImageResource;
    }

    public void setmImageResource(String mImageResource) {
        this.mImageResource = mImageResource;
    }
}
