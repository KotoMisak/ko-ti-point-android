package cz.koto.misak.kotipoint.android.base.client.rest;


import java.util.List;

import cz.koto.misak.kotipoint.android.base.entity.KoTiEvent;
import retrofit.http.GET;
import rx.Observable;

public interface KoTiNodeApi {


    @GET("/api/kotinode/event")
    Observable<List<KoTiEvent>> eventList();

}
